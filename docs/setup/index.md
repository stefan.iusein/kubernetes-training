# Tutorial: Setting Up VirtualBox (on Windows), Docker, KIND and kubectl client (on Debian VM)

In this tutorial, we'll cover the steps to set up VirtualBox on a Windows machine, import a Virtual Machine (VM) template with a bridged network configuration, install Docker on a Debian VM, and finally install KIND (Kubernetes IN Docker) for local Kubernetes cluster management.

## Prerequisites

- Windows machine with administrative privileges
- Stable internet connection

## Steps

### 1. Installing VirtualBox on Windows

VirtualBox is a free and open-source virtualization software that allows you to run multiple operating systems simultaneously on a single machine.

1. Go to the [VirtualBox downloads page](https://www.virtualbox.org/wiki/Downloads).
2. Download the installer for Windows.
3. Run the installer and follow the on-screen instructions to complete the installation.
4. Once installed, launch VirtualBox.

### 2. Importing a VM Template with Bridged Network

Now, let's import a Virtual Machine template with a bridged network configuration.

1. Obtain a VM template file (usually in OVA or OVF format):

    1. If want install Docker, Kind, kubectl, download this-> [Kind-to-insall](https://1drv.ms/u/s!Au23xnnnl6ptkJlrQctkiQq8dxQdow?e=oFLJew)

    2. If you want to have a working cluster, download this -> [Kind-ready](https://1drv.ms/u/s!Au23xnnnl6ptkJls92NVFNAG08EapA?e=R3Bket)

2. In VirtualBox, go to `File -> Import Appliance`.
3. Browse and select the VM template file.
4. Follow the import wizard, ensuring to select "Reinitialize the MAC address of all network cards" and "Import Hard Drives as VDI" options.
5. After importing, select the imported VM from the VirtualBox interface and go to `Settings -> Network`.
6. Under Adapter 1, select "Bridged Adapter" in the Attached to dropdown menu.
7. Choose your network adapter from the Name dropdown.
8. Click OK to save the settings.

### 3. Connecting to your Debian VM

We'll now install Docker in the Debian VM.

1. Start the Debian VM in VirtualBox.
2. Login to the Debian VM from VirtualBox window using username: student and password: student2024
3. Aquire the ip of the VM
   ```bash
   student@kind:~/kubernetes-training/docs/setup$ ip a s enp0s3
   2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:e1:9d:81 brd ff:ff:ff:ff:ff:ff
    inet 172.30.0.148/24 brd 172.29.0.255 scope global dynamic enp0s3
       valid_lft 86319sec preferred_lft 86319sec
    inet6 fe80::a00:27ff:fee1:9d81/64 scope link
       valid_lft forever preferred_lft forever
   ```
4. You are interested in this line:
   ```bash
   inet 172.30.0.148/24 brd 172.29.0.255 scope global dynamic enp0s3
   ```
   However, the ip address may be different but you want to get something like this 172.30.0.148
5. Grab an SSH client. One option can be Putty [Putty Download Link](https://the.earth.li/~sgtatham/putty/latest/w64/putty.exe)
   Now open putty and insert in the `Host Name (or IP address)` field the ip obtained at the previous step.
   Click Accept on the next window and login using the username and password provided at step 2.
6. From now on you will be using the Putty SSH client to connect to your VM (or your prefered ssh client). Not using a SSH client will prevent you from copying/pasting commands or text to your VM.

### 4. Installing Docker in Debian VM

1. Download Docker installation script:
   ```bash
   curl -fsSL https://get.docker.com -o get-docker.sh
   ```
2. Install Docker by running:
   ```bash
   sh get-docker.sh
   ```
3. Add student user to docker group:
   ```bash
   usermod -aG docker student
   ```
### 5. Installing KIND
 1. Install KIND using the provided script:
   ```bash
   curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.11.1/kind-linux-amd64
   chmod +x ./kind
   sudo mv ./kind /usr/local/bin/kind
   ```
### 6. Installing kubectl
 1. Install kubectl client using the provided script:
   ```bash
   curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
   sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl

   ```
 2. Install and enable kubectl autocompletion for bash:
   ```bash
   sudo apt-get install bash-completion
   echo 'source <(kubectl completion bash)' >>~/.bashrc
   source ~/.bashrc
   ```


